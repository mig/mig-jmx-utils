#
# rpm spec for mig-jmx-utils
#

%define real_version %((cat %{_sourcedir}/VERSION || cat %{_builddir}/VERSION || echo UNKNOWN) 2>/dev/null)

Name:		mig-jmx-utils
Version:	%{real_version}
Release:	1%{?dist}
Summary:	JMX utilities for the CERN Messaging Team
License:	Apache-2.0
Group:		Applications/Internet
URL:		https://gitlab.cern.ch/mig/%{name}
Source0:	%{name}-%{version}.tgz
Source1:	VERSION
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
BuildRequires:	ant, java-1.8.0, java-sdk-1.8.0, perl

%description
This package contains the following set of JMX utilities:
  jmx-activemq - JMX client to interact with ActiveMQ brokers
  jmx-artemis  - JMX client to interact with Artemis brokers
  jmx-term     - Interactive command line JMX client

%prep
%setup -q -n %{name}-%{version}

%build
make build

%install
rm -fr %{buildroot}
make install INSTROOT=%{buildroot} INSTBINDIR=%{buildroot}%{_bindir} INSTMANDIR=%{buildroot}%{_mandir}

%clean
make clean
rm -fr %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES LICENSE README
/usr/share/%{name}
%{_bindir}/*
%{_mandir}/man?/*
