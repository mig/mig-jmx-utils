package ch.cern.mig.jmx;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Caching IP address resolver.
 */
public class CachingResolver {

    private static Map<String, String> ip2name = new HashMap<String, String>();

    public String resolve(final String ip) {
        String name = ip2name.get(ip);
        if (name == null) {
            try {
                name = InetAddress.getByName(ip).getHostName().toLowerCase(Locale.ENGLISH);
            } catch (UnknownHostException e) {
                name = ip;
            }
            ip2name.put(ip, name);
        }
        return name;
    }

}
