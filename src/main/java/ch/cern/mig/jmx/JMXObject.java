package ch.cern.mig.jmx;

import java.util.HashMap;
import java.util.Map;

import javax.management.ObjectName;

/**
 * JMX object with a name and string attributes.
 */
public class JMXObject {

    private final ObjectName name;
    private final Map<String, String> attr;

    public JMXObject(final ObjectName name) {
        this.name = name;
        this.attr = new HashMap<String, String>();
    }

    public ObjectName getName() {
        return name;
    }

    public void putAttr(final String key, final String val) {
        attr.put(key, val);
    }

    public String getAttr(final String key) {
        return attr.get(key);
    }

    public Map<String, String> getMap() {
        return attr;
    }

    public String toString() {
        return name.toString() + attr.toString();
    }

}
