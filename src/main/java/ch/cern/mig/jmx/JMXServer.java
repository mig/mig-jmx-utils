package ch.cern.mig.jmx;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JMX server abstraction.
 */
public class JMXServer {

    private static final Logger logger = LoggerFactory.getLogger(JMXServer.class);

    protected static final String TYPE_BOOLEAN = "java.lang.Boolean";
    protected static final String TYPE_STRING = "java.lang.String";

    private static CachingResolver dns = new CachingResolver();

    private final String name;
    private final String host;
    private final int port;
    private final int timeout;
    private final Map<String, Object> environment;
    private final boolean numeric;

    private ExecutorService executor;
    private JMXConnector jmxc;
    private MBeanServerConnection mbsc;

    public JMXServer(final String name, final String host, final int port,
                     final int timeout, final Map<String, Object> environment,
                     final boolean numeric) {
        this.name = name;
        this.host = host;
        this.port = port;
        this.timeout = timeout;
        this.environment = environment;
        this.numeric = numeric;
        executor = Executors.newCachedThreadPool();
    }

    public JMXServer(final String name, final JMXClient client) {
        this(name, client.getHost(name), client.getPort(name),
             client.getTimeout(name), client.getEnv(name), client.isNumeric());
    }

    public String resolve(final String ip) {
        return numeric ? ip : dns.resolve(ip);
    }

    public String getName() {
        return name;
    }

    public boolean isConnected() {
        return jmxc != null && mbsc != null;
    }

    public void connect() throws JMXException {
        try {
            logger.debug("connecting {}:{}...", host, port);
            String url = "service:jmx:rmi:///jndi/rmi://" + host + ":" + port + "/jmxrmi";
            JMXServiceURL jsu = new JMXServiceURL(url);
            jmxc = JMXConnectorFactory.connect(jsu, environment);
            mbsc = jmxc.getMBeanServerConnection();
        } catch (IOException e) {
            jmxc = null;
            mbsc = null;
            throw new JMXException("cannot connect to " + host + ":" + port, e);
        }
    }

    public void disconnect() throws JMXException {
        if (!isConnected()) {
            return;
        }
        executor.shutdownNow();
        mbsc = null;
        try {
            logger.debug("disconnecting {}:{}...", host, port);
            jmxc.close();
            jmxc = null;
        } catch (IOException e) {
            jmxc = null;
            throw new JMXException("cannot disconnect from " + host + ":" + port, e);
        }
    }

    public Set<ObjectName> queryNames(final ObjectName on) throws JMXException {
        if (!isConnected()) {
            throw new JMXException("not connected");
        }
        try {
            return mbsc.queryNames(on, null);
        } catch (IOException e) {
            throw new JMXException("cannot queryNames of " + on, e);
        }
    }

    public Object getAttribute(final ObjectName on, final String attr)
        throws JMXException {
        if (!isConnected()) {
            throw new JMXException("not connected");
        }
        try {
            return mbsc.getAttribute(on, attr);
        } catch (AttributeNotFoundException | InstanceNotFoundException
                 | IOException | MBeanException | ReflectionException e) {
            throw new JMXException("cannot getAttribute of " + on, e);
        }
    }

    public Map<String, String> getAttributes(final ObjectName on, final String[] attrs)
        throws JMXException {
        if (!isConnected()) {
            throw new JMXException("not connected");
        }
        try {
            AttributeList al = mbsc.getAttributes(on, attrs);
            Map<String, String> map = new HashMap<String, String>();
            for (Attribute a: al.asList()) {
                map.put(a.getName(), a.getValue().toString());
            }
            return map;
        } catch (InstanceNotFoundException | IOException | ReflectionException e) {
            throw new JMXException("cannot getAttributes of " + on, e);
        }
    }

    public Object invoke(final ObjectName on, final String op,
                         final Object[] params, final String[] signature)
        throws JMXException {
        if (!isConnected()) {
            throw new JMXException("not connected");
        }
        Callable<Object> task = new JMXCallable(on, op, params, signature);
        try {
            Future<Object> future = executor.submit(task);
            return future.get(timeout, TimeUnit.SECONDS);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            throw new JMXException("cannot invoke " + op + " on " + on, e);
        }
    }

    /**
     * Helper class to be able to invoke a remote method with a timeout.
     */
    private class JMXCallable implements Callable<Object> {

        private ObjectName on;
        private String op;
        private Object[] params;
        private String[] signature;

        JMXCallable(final ObjectName on, final String op,
                    final Object[] params, final String[] signature) {
            this.on = on;
            this.op = op;
            this.params = params;
            this.signature = signature;
        }

        public Object call()
            throws IOException, InstanceNotFoundException, MBeanException, ReflectionException {
            return mbsc.invoke(on, op, params, signature);
        }

    }

}
