package ch.cern.mig.jmx;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Artemis JMX server abstraction.
 */
public class ArtemisServer extends JMXServer {

    private static final Logger logger = LoggerFactory.getLogger(ArtemisServer.class);
    private static final String DOMAIN = "org.apache.activemq.artemis";
    private static final String ADDRESS_PREFIX = "/";
    private static final int ADDRESS_PREFIX_LENGTH = ADDRESS_PREFIX.length();

    private static JSONParser json = new JSONParser();

    private String brokerName;
    private ObjectName coreObject;
    private ObjectName jmsObject;

    //
    // Artemis specific JMX
    //

    public ArtemisServer(final String name, final ArtemisClient client) {
        super(name, (JMXClient) client);
    }

    @Override
    public void connect() throws JMXException {
        ObjectName on;
        try {
            on = new ObjectName(DOMAIN + ":type=Broker,module=Core,serviceType=Server,*");
        } catch (MalformedObjectNameException e) {
            throw new JMXException("malformed object name!", e);
        }
        super.connect();
        Set<ObjectName> names = queryNames(on);
        if (names.size() != 1) {
            throw new JMXException("unexpected Artemis MBeans count: " + names.size());
        }
        coreObject = names.toArray(new ObjectName[0])[0];
        Pattern pat = Pattern.compile("[:,]brokerName=\"([a-zA-Z0-9\\.\\:\\-\\_]+)\",");
        Matcher mat = pat.matcher(coreObject.toString());
        if (mat.find()) {
            brokerName = mat.group(1);
            logger.debug("set brokerName={}", brokerName);
        } else {
            throw new JMXException("unexpected Artemis MBean: " + coreObject);
        }
        try {
            jmsObject = new ObjectName(coreObject.toString()
                                       .replace(",module=Core,", ",module=JMS,"));
        } catch (MalformedObjectNameException e) {
            throw new JMXException("malformed object name!", e);
        }
    }

    //
    // connection handling
    //

    @SuppressWarnings("unchecked")
    public List<JMXObject> listConnections() throws JMXException {
        Object[] args = {};
        String[] sigs = {};
        String[] addrs = (String[]) invoke(jmsObject, "listRemoteAddresses", args, sigs);
        List<JMXObject> list = new LinkedList<>();
        for (String addr: addrs) {
            JMXObject obj = new JMXObject(jmsObject);
            obj.putAttr("entity", getName());
            // expecting /<address>:<port>
            int colidx = addr.indexOf(':', ADDRESS_PREFIX_LENGTH);
            if (addr.startsWith(ADDRESS_PREFIX) && colidx > 0) {
                obj.putAttr("address", addr.substring(ADDRESS_PREFIX_LENGTH, colidx));
                obj.putAttr("port", addr.substring(colidx + 1));
                obj.putAttr("hostname", resolve(obj.getAttr("address")));
                obj.putAttr("addrport", obj.getAttr("address") + ":" + obj.getAttr("port"));
                obj.putAttr("hostport", obj.getAttr("hostname") + ":" + obj.getAttr("port"));
            } else {
                throw new JMXException("unexpected remote address: " + addr);
            }
            list.add(obj);
        }
        return list;
    }

    public void deleteConnection(final JMXObject obj) throws JMXException {
        // expecting /<address>:<port>
        Object[] args = {ADDRESS_PREFIX + obj.getAttr("addrport")};
        String[] sigs = {TYPE_STRING};
        invoke(jmsObject, "closeConnectionsForAddress", args, sigs);
    }

    //
    // destination handling
    //

    @SuppressWarnings("unchecked")
    public List<JMXObject> listDestinations() throws JMXException {
        List<JMXObject> list = new LinkedList<>();
        Map<String, String> map;
        ObjectName on;
        String[] attrs;
        // queues
        try {
            on = new ObjectName(DOMAIN + ":type=Broker,brokerName=\"" + brokerName
                                + "\",module=JMS,serviceType=Queue,*");
        } catch (MalformedObjectNameException e) {
            throw new JMXException("malformed object name!", e);
        }
        attrs = new String[] {"Name", "ConsumerCount", "MessagesAdded", "DeliveringCount",
                              "MessageCount"};
        for (ObjectName name: queryNames(on)) {
            JMXObject obj = new JMXObject(name);
            obj.putAttr("entity", getName());
            try {
                map = getAttributes(name, attrs);
            } catch (JMXException e) {
                Throwable cause = e.getRootCause();
                logger.debug("caught {}", cause.toString());
                String why = cause.getClass().getSimpleName();
                if (why.equals("InstanceNotFoundException")) {
                    continue;
                } else {
                    throw e;
                }
            }
            obj.putAttr("name", map.get("Name"));
            obj.putAttr("destination", "/queue/" + map.get("Name"));
            obj.putAttr("consumers", map.get("ConsumerCount"));
            obj.putAttr("enqueued", map.get("MessagesAdded"));
            obj.putAttr("pending", map.get("DeliveringCount"));
            obj.putAttr("size", map.get("MessageCount"));
            list.add(obj);
        }
        // topics
        try {
            on = new ObjectName(DOMAIN + ":type=Broker,brokerName=\"" + brokerName
                                + "\",module=JMS,serviceType=Topic,*");
        } catch (MalformedObjectNameException e) {
            throw new JMXException("malformed object name!", e);
        }
        attrs = new String[] {"Name", "SubscriptionCount", "MessagesAdded", "DeliveringCount",
                              "MessageCount"};
        for (ObjectName name: queryNames(on)) {
            JMXObject obj = new JMXObject(name);
            obj.putAttr("entity", getName());
            try {
                map = getAttributes(name, attrs);
            } catch (JMXException e) {
                Throwable cause = e.getRootCause();
                logger.debug("caught {}", cause.toString());
                String why = cause.getClass().getSimpleName();
                if (why.equals("InstanceNotFoundException")) {
                    continue;
                } else {
                    throw e;
                }
            }
            obj.putAttr("name", map.get("Name"));
            obj.putAttr("destination", "/topic/" + map.get("Name"));
            obj.putAttr("consumers", map.get("SubscriptionCount"));
            obj.putAttr("enqueued", map.get("MessagesAdded"));
            obj.putAttr("pending", map.get("DeliveringCount"));
            obj.putAttr("size", map.get("MessageCount"));
            list.add(obj);
        }
        return list;
    }

    public void deleteDestination(final JMXObject obj) throws JMXException {
        Object[] args = {obj.getAttr("name"), true};
        String[] sigs = {TYPE_STRING, TYPE_BOOLEAN};
        if (obj.getAttr("destination").startsWith("/queue/")) {
            invoke(jmsObject, "destroyQueue", args, sigs);
        } else {
            invoke(jmsObject, "destroyTopic", args, sigs);
        }
    }

}
