package ch.cern.mig.jmx;

/**
 * JMX exception.
 */
public class JMXException extends Exception {

    private static final long serialVersionUID = 1L;

    public JMXException(final String message) {
        super(message);
    }

    public JMXException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public Throwable getRootCause() {
        Throwable result = this;
        while (true) {
            Throwable cause = result.getCause();
            if (cause != null && cause != result) {
                result = cause;
            } else {
                break;
            }
        }
        return result;
    }

}
