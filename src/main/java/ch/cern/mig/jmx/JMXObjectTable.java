package ch.cern.mig.jmx;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Table of JMX objects.
 */
public class JMXObjectTable {

    private final String[] columns;
    private final List<JMXObject> objects;

    public JMXObjectTable(final String[] columns) {
        this.columns = columns.clone();
        this.objects = new LinkedList<>();
    }

    public void add(final JMXObject obj) {
        objects.add(obj);
    }

    public void add(final List<JMXObject> objs) {
        objects.addAll(objs);
    }

    public void display() {
        // compute the maximum width of each colmun
        Map<String, Integer> width = new HashMap<String, Integer>();
        for (String col: columns) {
            width.put(col, col.length());
        }
        for (JMXObject obj: objects) {
            for (String col: columns) {
                int len = obj.getAttr(col).length();
                if (len > width.get(col)) {
                    width.put(col, len);
                }
            }
        }
        // compute the format of each colmun
        Map<String, String> format = new HashMap<String, String>();
        for (String col: columns) {
            format.put(col, "%-" + (width.get(col) + 2) + "s");
        }
        // format the header
        StringBuilder sb = new StringBuilder();
        for (String col: columns) {
            sb.append(String.format(format.get(col), col.toUpperCase(Locale.ENGLISH)));
        }
        System.out.println(sb.toString().replaceFirst("\\s+$", ""));
        // format each line
        for (JMXObject obj: objects) {
            sb.setLength(0);
            for (String col: columns) {
                sb.append(String.format(format.get(col), obj.getAttr(col)));
            }
            System.out.println(sb.toString().replaceFirst("\\s+$", ""));
        }
    }

}
