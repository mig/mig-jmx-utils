package ch.cern.mig.jmx;

import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * Convenient JSON parser (using the Nashorn extensions).
 */
public class JSONParser {

    private static ScriptEngineManager mgr = new ScriptEngineManager();
    private static ScriptEngine engine = mgr.getEngineByName("nashorn");

    @SuppressWarnings("unchecked")
    public List<Object> parseAsList(final String json) throws JMXException {
        Object res;
        try {
            res = engine.eval("Java.asJSONCompatible(" + json + ")");
        } catch (ScriptException e) {
            throw new JMXException("invalid JSON: " + json, e);
        }
        if (res instanceof List) {
            return (List<Object>) res;
        } else {
            throw new JMXException("invalid JSON array: " + json);
        }
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> parseAsMap(final String json) throws JMXException {
        Object res;
        try {
            res = engine.eval("Java.asJSONCompatible(" + json + ")");
        } catch (ScriptException e) {
            throw new JMXException("invalid JSON: " + json, e);
        }
        if (res instanceof Map) {
            return (Map<String, Object>) res;
        } else {
            throw new JMXException("invalid JSON object: " + json);
        }
    }

}
