package ch.cern.mig.jmx;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Versatile string matcher.
 */
public class StringMatcher {

    private Boolean result;
    private Set<String> strings;
    private List<Pattern> patterns;

    public StringMatcher(final boolean pattern, final List<String> list) {
        if (pattern) {
            // will match if any pattern matches
            patterns = new LinkedList<>();
            for (String str: list) {
                patterns.add(Pattern.compile(str));
            }
        } else {
            // will match if any string matches
            strings = new HashSet<String>();
            for (String str: list) {
                strings.add(str);
            }
        }
    }

    public StringMatcher(final boolean pattern, final String... array) {
        this(pattern, Arrays.asList(array));
    }

    public StringMatcher(final boolean result) {
        this.result = result;
    }

    public boolean match(final String str) {
        if (result != null) {
            // static behaviour
            return result;
        }
        if (strings != null) {
            // dynamic string matching
            return strings.contains(str);
        }
        if (patterns != null) {
            // dynamic pattern matching
            for (Pattern pat: patterns) {
                if (pat.matcher(str).find()) {
                    return true;
                }
            }
        }
        return false;
    }

}
