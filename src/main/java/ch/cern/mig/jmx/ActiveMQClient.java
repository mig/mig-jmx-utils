package ch.cern.mig.jmx;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

/**
 * ActiveMQ JMX client abstraction.
 */
@Command(name = "jmx-activemq",
         description = "JMX client to interact with ActiveMQ brokers")
public class ActiveMQClient extends JMXClient implements Callable<Integer> {

    private static final Logger logger = LoggerFactory.getLogger(ActiveMQClient.class);
    private static final int LISTER_THREADS = 7;
    private static final String[] CONNECTION_COLUMNS
        = new String[] {"id", "entity", "port", "address", "hostname", "active", "slow"};
    private static final String[] DESTINATION_COLUMNS
        = new String[] {"id", "entity", "memory", "size", "enqueued", "dequeued", "pending",
                        "consumers", "destination"};

    private ConditionEvaluator evaluator;
    private StringMatcher matcher;
    private String[] typeColumns;

    @Option(names = {"-C", "--condition"},
            description = "set the condition used to filter the received JMX objects")
    private String optCondition;

    @Option(names = {"-f", "--force"},
            description = "execute the operations on the JMX objects without any confirmation")
    private boolean optForce;

    @Option(names = {"-o", "--operation"},
            description = "set the operation to execute on each selected object")
    private String optOperation;

    @Option(names = {"-r", "--regexp"},
            description = "treat the given arguments as regular expressions")
    private boolean optRegexp;

    @Option(names = {"-t", "--type"},
            description = "set the type of objects to operate on")
    private String optType = "destination";

    @Parameters()
    private List<String> arguments;

    private boolean matchArgs(final String str) {
        logger.trace("argument matching for {}", str);
        return matcher.match(str);
    }

    private boolean matchCond(final Map<String, String> map) throws JMXException {
        logger.trace("condition matching for {}", map);
        return evaluator.eval(map);
    }

    private void deleteObject(final ActiveMQServer server, final JMXObject obj) {
        try {
            if (optType.equals("destination")) {
                System.out.printf("deleting destination %s (%s)... ",
                                  obj.getAttr("id"), obj.getAttr("destination"));
                server.deleteDestination(obj);
            } else if (optType.equals("connection")) {
                System.out.printf("deleting connection %s (%s)... ",
                                  obj.getAttr("id"), obj.getAttr("hostport"));
                server.deleteConnection(obj);
            }
            System.out.println("ok");
        } catch (JMXException e) {
            logger.debug("caught {}", e);
            String why = e.getRootCause().getClass().getSimpleName();
            System.out.printf("failed [%s]%n", why);
        }
    }

    private void delete(final Map<String, ActiveMQServer> asm, final List<JMXObject> list) {
        JMXObjectTable table = new JMXObjectTable(typeColumns);
        table.add(list);
        table.display();
        StringMatcher sm;
        if (optForce) {
            // will delete all
            sm = new StringMatcher(true);
        } else {
            // will delete some
            String line = System.console().readLine("%nIDs to delete: ");
            if (line == null) {
                return;
            }
            sm = new StringMatcher(false, line.trim().split("[^0-9]+"));
        }
        System.out.println();
        for (JMXObject obj: list) {
            if (sm.match(obj.getAttr("id"))) {
                deleteObject(asm.get(obj.getAttr("entity")), obj);
            }
        }
    }

    private void display(final List<JMXObject> list) throws JMXException {
        // skip the first column (id)
        String[] columns = Arrays.copyOfRange(typeColumns, 1, typeColumns.length);
        JMXObjectTable table = new JMXObjectTable(columns);
        table.add(list);
        table.display();
    }

    public void run() throws JMXException {
        Collection<ObjectLister> listers = new ArrayList<ObjectLister>();
        Map<String, ActiveMQServer> asm = new HashMap<String, ActiveMQServer>();
        for (String name: getNames()) {
            ActiveMQServer server = new ActiveMQServer(name, this);
            listers.add(new ObjectLister(server));
            asm.put(name, server);
        }
        List<Future<List<JMXObject>>> futures;
        try {
            ExecutorService executor = Executors.newFixedThreadPool(LISTER_THREADS);
            futures = executor.invokeAll(listers);
            executor.shutdown();
        } catch (InterruptedException e) {
            throw new JMXException("interrupted", e);
        }
        List<JMXObject> list = new LinkedList<>();
        int id = 0;
        for (Future<List<JMXObject>> future: futures) {
            try {
                for (JMXObject obj: future.get()) {
                    obj.putAttr("id", String.valueOf(++id));
                    list.add(obj);
                }
            } catch (InterruptedException e) {
                throw new JMXException("interrupted", e);
            } catch (ExecutionException e) {
                Throwable cause = e.getCause();
                if (cause instanceof JMXException) {
                    throw (JMXException) cause;
                } else {
                    throw new JMXException("unexpected ExecutionException", e);
                }
            }
        }
        if (list.size() == 0) {
            System.out.println("No items found.");
        } else {
            logger.debug("listed {} objects", list.size());
            if (optOperation == null) {
                display(list);
            } else if (optOperation.equals("delete")) {
                delete(asm, list);
            }
        }
        for (String name: getNames()) {
            asm.get(name).disconnect();
        }
    }

    public void setup() throws JMXException {
        if (optOperation != null && !optOperation.equals("delete")) {
            throw new JMXException("unexpected operation: " + optOperation);
        }
        if (optType.equals("connection")) {
            typeColumns = CONNECTION_COLUMNS;
        } else if (optType.equals("destination")) {
            typeColumns = DESTINATION_COLUMNS;
        } else {
            throw new JMXException("unexpected type: " + optType);
        }
        if (optCondition == null) {
            evaluator = new ConditionEvaluator(true);
        } else {
            evaluator = new ConditionEvaluator(optCondition);
        }
        if (arguments == null) {
            matcher = new StringMatcher(true);
        } else {
            matcher = new StringMatcher(optRegexp, arguments);
        }
    }

    @Override
    public Integer call() throws Exception {
        try {
            setUserConf(".jmx-activemq.conf");
            check();
            if (!isList()) {
                setup();
                run();
            }
        } catch (JMXException e) {
            if (logger.isDebugEnabled()) {
                e.printStackTrace();
            } else {
                System.err.printf("ActiveMQClient: %s%n", e.getRootCause().getMessage());
            }
            return 1;
        }
        return 0;
    }

    public static void main(final String... args) {
        int exitCode = new CommandLine(new ActiveMQClient()).execute(args);
        System.exit(exitCode);
    }

    /**
     * Helper class to be able to query brokers in parallel.
     */
    private class ObjectLister implements Callable<List<JMXObject>> {

        private ActiveMQServer server;

        ObjectLister(final ActiveMQServer server) {
            this.server = server;
        }

        @Override
        public List<JMXObject> call() throws JMXException {
            server.connect();
            long start = System.currentTimeMillis();
            List<JMXObject> list = new LinkedList<>();
            if (optType.equals("destination")) {
                for (JMXObject obj: server.listDestinations()) {
                    if (!matchArgs(obj.getAttr("destination"))) {
                        continue;
                    }
                    if (!matchCond(obj.getMap())) {
                        continue;
                    }
                    list.add(obj);
                }
            } else if (optType.equals("connection")) {
                for (JMXObject obj: server.listConnections()) {
                    if (!matchArgs(obj.getAttr("hostport"))
                        && !matchArgs(obj.getAttr("addrport"))) {
                        continue;
                    }
                    if (!matchCond(obj.getMap())) {
                        continue;
                    }
                    list.add(obj);
                }
            }
            long stop = System.currentTimeMillis();
            logger.debug("listed {} objects in {}ms", list.size(), stop - start);
            return list;
        }

    }

}
