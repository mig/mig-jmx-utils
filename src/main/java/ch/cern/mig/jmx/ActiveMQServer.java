package ch.cern.mig.jmx;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ActiveMQ JMX server abstraction.
 */
public class ActiveMQServer extends JMXServer {

    private static final Logger logger = LoggerFactory.getLogger(ActiveMQServer.class);
    private static final String DOMAIN = "org.apache.activemq";
    private static final String ADDRESS_PREFIX = "tcp://";
    private static final int ADDRESS_PREFIX_LENGTH = ADDRESS_PREFIX.length();

    private String brokerName;
    private ObjectName brokerObject;

    //
    // ActiveMQ specific JMX
    //

    public ActiveMQServer(final String name, final ActiveMQClient client) {
        super(name, (JMXClient) client);
    }

    @Override
    public void connect() throws JMXException {
        ObjectName on;
        try {
            on = new ObjectName(DOMAIN + ":type=Broker,service=Health,*");
        } catch (MalformedObjectNameException e) {
            throw new JMXException("malformed object name!", e);
        }
        super.connect();
        Set<ObjectName> names = queryNames(on);
        if (names.size() != 1) {
            // note: this can happen when interacting with an old ActiveMQ...
            throw new JMXException("unexpected ActiveMQ MBeans count: " + names.size());
        }
        on = names.toArray(new ObjectName[0])[0];
        Pattern pat = Pattern.compile("[:,][bB]rokerName=([a-zA-Z0-9\\.\\:\\-\\_]+),");
        Matcher mat = pat.matcher(on.toString());
        if (mat.find()) {
            brokerName = mat.group(1);
            logger.debug("set brokerName={}", brokerName);
        } else {
            throw new JMXException("unexpected ActiveMQ MBean: " + on);
        }
        try {
            brokerObject = new ObjectName(DOMAIN + ":type=Broker,brokerName=" + brokerName);
        } catch (MalformedObjectNameException e) {
            throw new JMXException("malformed object name!", e);
        }
    }

    //
    // connection handling
    //

    @SuppressWarnings("unchecked")
    public List<JMXObject> listConnections() throws JMXException {
        ObjectName on;
        try {
            on = new ObjectName(DOMAIN + ":type=Broker,brokerName=" + brokerName
                 + ",connector=clientConnectors,connectorName=*,connectionName=*,*");
        } catch (MalformedObjectNameException e) {
            throw new JMXException("malformed object name!", e);
        }
        Map<String, String> map;
        String[] attrs = new String[] {"Active", "RemoteAddress", "Slow"};
        List<JMXObject> list = new LinkedList<>();
        for (ObjectName name: queryNames(on)) {
            JMXObject obj = new JMXObject(name);
            obj.putAttr("entity", getName());
            try {
                map = getAttributes(name, attrs);
            } catch (JMXException e) {
                Throwable cause = e.getRootCause();
                logger.debug("caught {}", cause.toString());
                String why = cause.getClass().getSimpleName();
                if (why.equals("InstanceNotFoundException")) {
                    continue;
                } else {
                    throw e;
                }
            }
            obj.putAttr("active", map.get("Active"));
            obj.putAttr("slow", map.get("Slow"));
            String addr = map.get("RemoteAddress");
            if (addr.equalsIgnoreCase("null")) {
                continue;
            }
            // expecting tcp://<address>:<port>
            int colidx = addr.lastIndexOf(':');
            if (addr.startsWith(ADDRESS_PREFIX) && colidx > ADDRESS_PREFIX_LENGTH) {
                obj.putAttr("address", addr.substring(ADDRESS_PREFIX_LENGTH, colidx));
                obj.putAttr("port", addr.substring(colidx + 1));
                obj.putAttr("hostname", resolve(obj.getAttr("address")));
                obj.putAttr("addrport", obj.getAttr("address") + ":" + obj.getAttr("port"));
                obj.putAttr("hostport", obj.getAttr("hostname") + ":" + obj.getAttr("port"));
            } else {
                throw new JMXException("unexpected remote address: " + addr);
            }
            list.add(obj);
        }
        return list;
    }

    public void deleteConnection(final JMXObject obj) throws JMXException {
        invoke(obj.getName(), "stop", null, null);
    }

    //
    // destination handling
    //

    private void queryDestinations(final String prefix,
                                   final ObjectName[] names,
                                   final List<JMXObject> list)
        throws JMXException {
        String[] attrs = new String[] {"Name", "ConsumerCount", "DequeueCount", "EnqueueCount",
                                       "MemoryPercentUsage", "InFlightCount", "QueueSize"};
        for (ObjectName name: names) {
            JMXObject obj = new JMXObject(name);
            obj.putAttr("entity", getName());
            Map<String, String> map = getAttributes(name, attrs);
            obj.putAttr("name", map.get("Name"));
            obj.putAttr("destination", prefix + map.get("Name"));
            obj.putAttr("consumers", map.get("ConsumerCount"));
            obj.putAttr("dequeued", map.get("DequeueCount"));
            obj.putAttr("enqueued", map.get("EnqueueCount"));
            obj.putAttr("memory", map.get("MemoryPercentUsage"));
            obj.putAttr("pending", map.get("InFlightCount"));
            obj.putAttr("size", map.get("QueueSize"));
            list.add(obj);
        }
    }

    @SuppressWarnings("unchecked")
    public List<JMXObject> listDestinations() throws JMXException {
        ObjectName on;
        try {
            on = new ObjectName(DOMAIN + ":type=Broker,brokerName=" + brokerName);
        } catch (MalformedObjectNameException e) {
            throw new JMXException("malformed object name!", e);
        }
        List<JMXObject> list = new LinkedList<>();
        queryDestinations("/queue/", (ObjectName[]) getAttribute(on, "Queues"), list);
        queryDestinations("/topic/", (ObjectName[]) getAttribute(on, "Topics"), list);
        return list;
    }

    public void deleteDestination(final JMXObject obj) throws JMXException {
        Object[] args = {obj.getAttr("name")};
        String[] sigs = {TYPE_STRING};
        if (obj.getAttr("destination").startsWith("/queue/")) {
            invoke(brokerObject, "removeQueue", args, sigs);
        } else {
            invoke(brokerObject, "removeTopic", args, sigs);
        }
    }

}
