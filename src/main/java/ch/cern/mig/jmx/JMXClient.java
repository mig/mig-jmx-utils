package ch.cern.mig.jmx;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import picocli.CommandLine.Option;

/**
 * JMX client abstraction.
 */
public class JMXClient {

    private static final Logger logger = LoggerFactory.getLogger(JMXClient.class);
    private static final String ALL = "ALL";
    private static final String DEFAULT = "DEFAULT";
    private static final String CLINAME = "-";
    private static final Integer DEFAULT_PORT = 1099;
    private static final Integer DEFAULT_TIMEOUT = 30;

    private Set<String> optNames = new HashSet<String>();
    private Properties confProps = new Properties();
    private String defConf = ".jmxclt.conf";

    @Option(names = {"-c", "--conf"},
            description = "set the configuration file to use")
    private String optConf;

    @Option(names = {"-h", "--help"}, usageHelp = true,
            description = "show some help")
    private boolean optHelp;

    @Option(names = {"-H", "--host"},
            description = "set the host name to connect to")
    private String optHost;

    @Option(names = {"-l", "--list"},
            description = "show all the names defined in the configuration file")
    private boolean optList;

    @Option(names = {"-N", "--name"},
            description = "set the name(s) of the JMX servers to connect to")
    private String optName;

    @Option(names = {"-n", "--numeric"},
            description = "show numerical addresses instead of trying to resolve host names")
    private boolean optNumeric;

    @Option(names = {"-p", "--password"},
            description = "set the password to use during JMX authentication")
    private String optPassword;

    @Option(names = {"-P", "--port"},
            description = "set the port number to connect to")
    private Integer optPort;

    @Option(names = {"-T", "--timeout"},
            description = "set the method invocation timeout")
    private Integer optTimeout;

    @Option(names = {"-u", "--user"},
            description = "set the user name to use during JMX authentication")
    private String optUser;

    // accessors by name
    public List<String> getNames() {
        List<String> list = Arrays.asList(optNames.toArray(new String[0]));
        Collections.sort(list);
        return list;
    }
    public String getHost(final String name) {
        return getConfString(name, "host", optHost);
    }
    public String getPassword(final String name) {
        return getConfString(name, "password", optPassword);
    }
    public Integer getPort(final String name) {
        Integer port = getConfInteger(name, "port", optPort);
        return port == null ? DEFAULT_PORT : port;
    }
    public Integer getTimeout(final String name) {
        Integer timeout = getConfInteger(name, "timeout", optTimeout);
        return timeout == null ? DEFAULT_TIMEOUT : timeout;
    }
    public String getUser(final String name) {
        return getConfString(name, "user", optUser);
    }
    public Map<String, Object> getEnv(final String name) {
        String user = getUser(name);
        String pass = getPassword(name);
        if (user == null || user.isEmpty() || pass == null || pass.isEmpty()) {
            return null;
        }
        Map<String, Object> env = new HashMap<String, Object>();
        env.put("jmx.remote.credentials", new String[] {user, pass});
        return env;
    }
    public boolean isList() {
        return optList;
    }
    public boolean isNumeric() {
        return optNumeric;
    }

    // setter to change the name of the default configuration file
    protected void setUserConf(final String value) {
        logger.debug("set defConf={}", value);
        defConf = value;
    }

    // check the consistency of the command line arguments
    public void check() throws JMXException {
        logger.debug("checking arguments...");
        if (optConf == null) {
            // if configuration not given, use default one
            optConf = System.getProperty("user.home") + File.separatorChar + defConf;
        } else {
            // if configuration given, make sure the file exists
            if (!new File(optConf).exists()) {
                throw new JMXException("missing configuration file: " + optConf);
            }
        }
        if (new File(optConf).exists()) {
            // if configuration exists, load it
            logger.debug("reading {}...", optConf);
            try (
                FileInputStream fis = new FileInputStream(optConf);
            ) {
                confProps.load(fis);
            } catch (IOException e) {
                throw new JMXException("cannot load " + optConf, e);
            }
        }
        if (optList) {
            // if --list is given, only considers the configuration file
            if (confProps.size() == 0) {
                throw new JMXException("missing configuration file");
            }
            String[] names = knownNames().toArray(new String[0]);
            Arrays.sort(names);
            for (String name: names) {
                System.out.println(name);
            }
        } else if (optHost == null) {
            // if host name not given, use configuration
            if (confProps.size() == 0) {
                throw new JMXException("missing host name or configuration file");
            }
            if (optName == null) {
                throw new JMXException("missing configured name");
            }
            checkName();
        } else {
            // if host name given, get everything from command line arguments
            if (optUser == null) {
                throw new JMXException("missing user name");
            }
            if (optPassword == null) {
                char[] passwd = System.console()
                    .readPassword("Password for %s@%s: ", optUser, optHost);
                if (passwd == null) {
                    throw new JMXException("missing user password");
                }
                optPassword = new String(passwd);
                if (optPassword.isEmpty()) {
                    throw new JMXException("missing user password");
                }
            }
            optNames.add(CLINAME);
        }
        logger.debug("set optNames={}", optNames);
    }

    //
    // private helpers
    //

    // check and handle the "name" option
    private void checkName() throws JMXException {
        Set<String> known = knownNames();
        if (optName.equals(ALL)) {
            optNames = known;
            return;
        }
        for (String name: optName.split(",")) {
            if (name.startsWith("/") && name.endsWith("/")) {
                String substr = name.substring(1, name.length() - 1);
                StringMatcher sm = new StringMatcher(true, substr);
                int matched = 0;
                for (String kn: known.toArray(new String[0])) {
                    if (sm.match(kn)) {
                        optNames.add(kn);
                        matched++;
                    }
                }
                if (matched == 0) {
                    throw new JMXException("no name matching: " + name);
                }
            } else if (known.contains(name)) {
                optNames.add(name);
            } else {
                throw new JMXException("unknown name: " + name);
            }
        }
    }

    // return the list of names known if the configuration
    private Set<String> knownNames() {
        Set<String> known = new HashSet<String>();
        for (String key: confProps.stringPropertyNames()) {
            String name = key.split("\\.", 2)[0];
            if (!name.equals(DEFAULT)) {
                known.add(name);
            }
        }
        return known;
    }

    // get a configuration item, as a string
    private String getConfString(final String name, final String key, final String value) {
        if (value == null) {
            String result = confProps.getProperty(name + "." + key);
            if (result == null) {
                result = confProps.getProperty(DEFAULT + "." + key);
            }
            return result;
        } else {
            return value;
        }
    }

    // get a configuration item, as an integer
    private Integer getConfInteger(final String name, final String key, final Integer value) {
        if (value == null) {
            String result = confProps.getProperty(name + "." + key);
            if (result == null) {
                result = confProps.getProperty(DEFAULT + "." + key);
            }
            return result == null ? null : Integer.valueOf(result);
        } else {
            return value;
        }
    }

}
