package ch.cern.mig.jmx;

import java.util.Map;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * Versatile boolean condition evaluator (JavaScript).
 */
public class ConditionEvaluator {

    private static ScriptEngineManager mgr = new ScriptEngineManager();
    private static ScriptEngine engine = mgr.getEngineByName("JavaScript");

    private Boolean result;
    private String condition;

    public ConditionEvaluator(final String condition) {
        this.condition = condition;
    }

    public ConditionEvaluator(final boolean result) {
        this.result = result;
    }

    public boolean eval(final Map<String, String> map) throws JMXException {
        if (result != null) {
            // static behaviour
            return result;
        }
        // dynamic behaviour
        Bindings bindings = engine.createBindings();
        for (Map.Entry<String, String> entry: map.entrySet()) {
            bindings.put(entry.getKey(), entry.getValue());
        }
        Object res;
        try {
            res = engine.eval(condition, bindings);
        } catch (ScriptException e) {
            throw new JMXException("invalid condition: " + condition, e);
        }
        if (res instanceof Boolean) {
            return (boolean) res;
        } else {
            throw new JMXException("unexpected condition result: " + res);
        }
    }

}
