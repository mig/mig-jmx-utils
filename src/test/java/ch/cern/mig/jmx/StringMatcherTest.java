package ch.cern.mig.jmx;

import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit tests for StringMatcher.
 */
public class StringMatcherTest {

    @Test
    public void always() {
        StringMatcher sm = new StringMatcher(true);
        Assert.assertTrue(sm.match("foo"));
        Assert.assertTrue(sm.match("bar"));
    }

    @Test
    public void never() {
        StringMatcher sm = new StringMatcher(false);
        Assert.assertFalse(sm.match("foo"));
        Assert.assertFalse(sm.match("bar"));
    }

    @Test
    public void strcmp() {
        List<String> list = new LinkedList<>();
        list.add("foo");
        list.add("bar");
        StringMatcher sm = new StringMatcher(false, list);
        Assert.assertTrue(sm.match("foo"));
        Assert.assertTrue(sm.match("bar"));
        Assert.assertFalse(sm.match("FOO"));
        Assert.assertFalse(sm.match("foo "));
        Assert.assertFalse(sm.match(" bar"));
        Assert.assertFalse(sm.match("fo"));
        Assert.assertFalse(sm.match("ar"));
    }

    @Test
    public void pattern1() {
        StringMatcher sm = new StringMatcher(true, "foo");
        Assert.assertTrue(sm.match("foo"));
        Assert.assertTrue(sm.match(" foo"));
        Assert.assertTrue(sm.match("foobar"));
        Assert.assertTrue(sm.match("fofoofo"));
        Assert.assertFalse(sm.match("FOO"));
    }

    @Test
    public void pattern2() {
        StringMatcher sm = new StringMatcher(true, "ba+r", "g.g");
        Assert.assertTrue(sm.match("bar"));
        Assert.assertTrue(sm.match("baaaar"));
        Assert.assertTrue(sm.match("barabarab"));
        Assert.assertTrue(sm.match("gag"));
        Assert.assertTrue(sm.match(" gog "));
        Assert.assertTrue(sm.match("g g"));
        Assert.assertFalse(sm.match("BAR"));
        Assert.assertFalse(sm.match("ggbrgg"));
    }

}
