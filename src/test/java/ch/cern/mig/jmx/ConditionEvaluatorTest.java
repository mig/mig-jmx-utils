package ch.cern.mig.jmx;

import java.util.HashMap;
import java.util.Map;

import javax.script.ScriptException;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit tests for ConditionEvaluator.
 */
public class ConditionEvaluatorTest {

    private Map<String, String> testMap(final String value) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("bar", "0");
        map.put("foo", value);
        return map;
    }

    @Test
    public void str1() throws JMXException {
        ConditionEvaluator ce = new ConditionEvaluator("foo=='bar'");
        Assert.assertFalse(ce.eval(testMap("BAR")));
        Assert.assertTrue(ce.eval(testMap("bar")));
    }

    @Test
    public void str2() throws JMXException {
        ConditionEvaluator ce = new ConditionEvaluator("foo>'bar'");
        Assert.assertFalse(ce.eval(testMap("ba")));
        Assert.assertTrue(ce.eval(testMap("bas")));
    }

    @Test
    public void good1() throws JMXException {
        ConditionEvaluator ce = new ConditionEvaluator("foo>1");
        Assert.assertFalse(ce.eval(testMap("0")));
        Assert.assertTrue(ce.eval(testMap("2")));
    }

    @Test
    public void good2() throws JMXException {
        ConditionEvaluator ce = new ConditionEvaluator("foo>=10");
        Assert.assertFalse(ce.eval(testMap("2")));
        Assert.assertTrue(ce.eval(testMap("10")));
    }

    @Test
    public void good3() throws JMXException {
        ConditionEvaluator ce = new ConditionEvaluator("foo>1 && foo<=2");
        Assert.assertFalse(ce.eval(testMap("0")));
        Assert.assertTrue(ce.eval(testMap("2")));
        Assert.assertFalse(ce.eval(testMap("3")));
    }

    @Test(expected = JMXException.class)
    public void bad1() throws JMXException {
        ConditionEvaluator ce = new ConditionEvaluator("foo>");
        Assert.assertFalse(ce.eval(testMap("0")));
    }

    @Test(expected = JMXException.class)
    public void bad2() throws JMXException {
        ConditionEvaluator ce = new ConditionEvaluator("FOO>1");
        Assert.assertFalse(ce.eval(testMap("0")));
    }

    @Test(expected = JMXException.class)
    public void bad3() throws JMXException {
        ConditionEvaluator ce = new ConditionEvaluator("foo+1");
        Assert.assertFalse(ce.eval(testMap("0")));
    }

    @Test(expected = JMXException.class)
    public void bad4() throws JMXException {
        ConditionEvaluator ce = new ConditionEvaluator("foo>0");
        Assert.assertTrue(ce.eval(testMap("1")));
        // make sure foo is forgotten...
        Assert.assertTrue(ce.eval(new HashMap<String, String>()));
    }

    @Test
    public void always() throws JMXException {
        ConditionEvaluator ce = new ConditionEvaluator(true);
        Assert.assertTrue(ce.eval(testMap("0")));
        Assert.assertTrue(ce.eval(testMap("9")));
    }

    @Test
    public void never() throws JMXException {
        ConditionEvaluator ce = new ConditionEvaluator(false);
        Assert.assertFalse(ce.eval(testMap("0")));
        Assert.assertFalse(ce.eval(testMap("9")));
    }

}
