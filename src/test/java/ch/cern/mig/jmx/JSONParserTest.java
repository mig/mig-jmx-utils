package ch.cern.mig.jmx;

import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit tests for JSONParser.
 */
public class JSONParserTest {

    JSONParser json = new JSONParser();

    @Test(expected=JMXException.class)
    public void badList() throws JMXException {
        List<Object> list = json.parseAsList("{}");
        Assert.assertTrue(list.isEmpty());
    }

    @Test
    public void emptyList() throws JMXException {
        List<Object> list = json.parseAsList("[]");
        Assert.assertTrue(list.isEmpty());
    }

    @Test
    public void simpleList() throws JMXException {
        List<Object> list = json.parseAsList("[1, 2, 3]");
        Assert.assertEquals(3, list.size());
    }

    @Test
    public void complexList() throws JMXException {
        List<Object> list = json.parseAsList("[1, {}, [2, 3, 4]]");
        Assert.assertEquals(3, list.size());
    }

    @Test(expected=JMXException.class)
    public void badMap() throws JMXException {
        Map<String, Object> map = json.parseAsMap("[]");
        Assert.assertTrue(map.isEmpty());
    }

    @Test
    public void emptyMap() throws JMXException {
        Map<String, Object> map = json.parseAsMap("{}");
        Assert.assertTrue(map.isEmpty());
    }

    @Test
    public void simpleMap() throws JMXException {
        Map<String, Object> map = json.parseAsMap("{'one': 1, 'two': 2, 'three': 3}");
        Assert.assertEquals(3, map.size());
    }

    @Test
    public void complexMap() throws JMXException {
        Map<String, Object> map = json.parseAsMap("{ x: 343, y: 'hello', z: [2,4, 5] }");
        Assert.assertEquals(3, map.size());
    }

}
