#
# Makefile for mig-jmx-utils
#

PKGNAME=mig-jmx-utils
PKGVERSION=$(shell cat VERSION)
PKGTAG=v${PKGVERSION}
FULLPKGNAME=${PKGNAME}-${PKGVERSION}
TARBALL=${FULLPKGNAME}.tgz
SCRIPTS=jmx-activemq jmx-artemis jmx-term

INSTROOT=
INSTBINDIR=${INSTROOT}/usr/bin
INSTLIBDIR=${INSTROOT}/usr/share/${PKGNAME}
INSTMANDIR=${INSTROOT}/usr/share/man

.PHONY: all sources build install tag clean

all:

sources: ${TARBALL}

${TARBALL}:
	[ -d dist ] && rm -fr dist || true
	mkdir -p dist/${FULLPKGNAME}
	cp -a CHANGES LICENSE Makefile README VERSION *.xml bin lib src dist/${FULLPKGNAME}
	cd dist; tar cvfz ../${TARBALL} ${FULLPKGNAME}
	rm -fr dist

build:
	[ -d man ] || mkdir man
	for name in ${SCRIPTS}; do \
	  pod2man --section=1 bin/$$name > man/$$name.1; \
	done
	ant -f build.xml

install:
	for name in ${SCRIPTS}; do \
	  install -D -m 755 bin/$$name   ${INSTBINDIR}/$$name; \
	  install -D -m 644 man/$$name.1 ${INSTMANDIR}/man1/$$name.1; \
	done
	for name in lib/*.jar; do \
	  install -D -m 644 $$name ${INSTLIBDIR}/`basename $$name`; \
	done
	install -D -m 644 build/jar/${PKGNAME}.jar ${INSTLIBDIR}/${FULLPKGNAME}.jar

tag:
	@seen=`git tag -l | grep -Fx ${PKGTAG}`; \
	if [ "x$$seen" = "x" ]; then \
	    set -x; \
	    git tag ${PKGTAG}; \
	    git push --tags; \
	else \
	    echo "already tagged with ${PKGTAG}"; \
	fi

clean:
	rm -fr ${TARBALL} build dist man target
